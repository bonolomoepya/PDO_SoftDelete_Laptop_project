<?php
include_once ("../../../vendor/autoload.php");
use App\Seip50\SoftDelete\Softdelete;

$obj = new Softdelete();
$obj->prepare($_GET)->show();
$obj->trashdelete($_GET);
<?php
include_once ("../../../vendor/autoload.php");
use App\Seip50\SoftDelete\Softdelete;

$obj = new Softdelete();
$data= $obj->prepare($_GET)->show();
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
<body>
    <?php include_once ("./header.php");?>
    <div>
            <h3>Laptop Data List</h3>

        <table>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Model</th>
            <th>Serial</th>
            <th>Color</th>
            <th>Price</th>
            <th>Purchase Date</th>
          </tr>
        
        <tr>
          <td><?php echo $data['id']; ?></td>
          <td><?php echo $data['title']; ?></td>
          <td><?php echo $data['model']; ?></td>
          <td><?php echo $data['sl'] ;?></td>
          <td><?php echo $data['color']; ?></td>
          <td><?php echo $data['price']; ?></td>
          <td><?php echo $data['pdate'];?></td>

        </tr>
        </table>
    </div>
</body>
</html>